/*
 * Copyright (C) 2023
 *     Mark O'Donovan <shiftee@posteo.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * This file provides an alternate reality where the Opta has an EEPROM
 */

#pragma once

#include <mbed.h>
#include "QSPIFBlockDevice.h"
#include "log.h"

QSPIFBlockDevice *_root = NULL;

void eeprom_info()
{
	log("Block device size      : %llu KB", _root->size()  / 1024);
	log("Erasable block size    : %llu KB", _root->get_erase_size() / 1024);
	log("Readable block size    : %llu bytes", _root->get_read_size());
	log("Programmable block size: %llu bytes", _root->get_program_size());
}

int eeprom_init()
{
	if ( ! _root )
		_root = new QSPIFBlockDevice(PD_11, PD_12, PF_7, PD_13, PF_10, PG_6,
									QSPIF_POLARITY_MODE_1, 40000000);

	if ( ! _root )
	{
		log("failed to alloc block device driver");
		return -1;
	}

	int status = _root->init();
	if ( status )
		log("Initing root device failed with status %d", status);

	return status;
}

int eeprom_deinit()
{
	int status = _root->deinit();
	if ( status )
		log("deinit() done with status %d", status);

	return 0;
}

/*
 * Returns 0 on success
 */
int eeprom_read(byte *buf, unsigned len)
{
	return _root->read(buf, 0, len);
}

/*
 * Returns 0 on success
 */
int eeprom_write(byte *buf, unsigned len)
{
	int32_t status = _root->erase(0, 4096);

	if ( status )
	{
		log("eeprom_write() erase done with status %d", status);
		return -1;
	}

	size_t block_size = _root->get_program_size();

	unsigned num_blocks = len / block_size;
	if ( len % block_size )
		num_blocks++;

	return _root->program(buf, 0, num_blocks * block_size);
}

