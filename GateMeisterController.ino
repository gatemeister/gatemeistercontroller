/*
 * Copyright (C) 2024
 *     Mark O'Donovan <shiftee@posteo.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * GateMeister MQTT gate controller
 *
 * Designed to run on the Arduino Opta PLC
 */

#include <Ethernet.h>
#include <PubSubClient.h>
#include "mbedtls/aes.h"
#include "OptaEEPROM.h"

enum userButtonEvent
{
	EVENT_USER_BTN_NONE = 0,
	EVENT_USER_BTN_PRESSED,
	EVENT_USER_BTN_RELEASED
};

enum gateCommand
{
	GATE_CMD_NONE = 0,
	GATE_CMD_OPEN,
	GATE_CMD_CLOSE
};

enum gateState
{
	GATE_STATE_CLOSED = 0,
	GATE_STATE_OPEN
};

struct encrypt_block
{
	uint8_t  data[10];
	uint8_t  len;
	uint8_t  rand[3];
	uint16_t crc;
};

struct cmd_block
{
	uint8_t  open_req;
	uint8_t  close_req;
	uint8_t  pad1;
	uint8_t  state_open;
	uint16_t token;
	uint8_t  pad[4];
};

enum modbus_exception_t
{
	MODBUS_EXCEPTION_BAD_FUNC = 1,
	MODBUS_EXCEPTION_BAD_ADDR,
	MODBUS_EXCEPTION_BAD_VALUE,
	MODBUS_EXCEPTION_FAILURE,
	MODBUS_NUM_EXCEPTIONS
};

enum modbus_reg_t
{
	MODBUS_REG_PROJECT_ID,
	MODBUS_REG_KEY0,
	MODBUS_REG_KEY1,
	MODBUS_REG_KEY2,
	MODBUS_REG_KEY3,
	MODBUS_REG_KEY4,
	MODBUS_REG_KEY5,
	MODBUS_REG_KEY6,
	MODBUS_REG_KEY7,
	MODBUS_NUM_REGS
};

struct config
{
	uint8_t  cfg_ver;
	uint8_t  key[16];
	uint8_t  pad[13];
	uint16_t crc;
};

/* global variables */
EthernetClient eth_client;
EthernetServer server(0);
PubSubClient mqtt_client(eth_client);
gateState gate_state;
gateCommand gate_cmd;
uint16_t gate_token;
struct config config;

uint16_t htons(uint16_t hostshort)
{
#if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
	uint8_t lo = (hostshort >> 0) & 0xFF;
	uint8_t hi = (hostshort >> 8) & 0xFF;
	return (lo << 8) | hi;
#else
	return hostshort;
#endif
}

uint16_t ntohs(uint16_t netshort)
{
	return htons(netshort);
}

/*
 * Calculates the CRC for num_bytes of buf
 */
uint16_t getCRC16(uint8_t* buf, unsigned num_bytes)
{
	uint16_t crc = 0xFFFF;

	for(unsigned i=0; i<num_bytes; i++)
	{
		crc ^= buf[i];

		for(int j=0; j<8; j++)
		{
			if( crc & 1 )
				crc = (crc >> 1) ^ 0xA001;
			else
				crc = (crc >> 1);
		}
	}

	return crc;
}

/*
 * Encrypt the passed input data block
 * Returns 0 on success
 */
int encryptData(uint8_t *input, uint8_t *output, uint8_t *key)
{
	mbedtls_aes_context aes;
	mbedtls_aes_init(&aes);

	int status = mbedtls_aes_setkey_enc( &aes, key, 128 );
	if ( status )
	{
		log("error: failed to set key with status %d", status);
		goto done;
	}

	status = mbedtls_aes_crypt_ecb( &aes, MBEDTLS_AES_ENCRYPT, input, output );
	if ( status )
	{
		log("error: failed to encrypt block with status %d", status);
		goto done;
	}

done:
	mbedtls_aes_free(&aes);

	return status;
}

/*
 * Decrypt the passed input data in a struct encrypt_block
 * Returns 0 on success
 */
int decryptData(uint8_t *input, uint8_t *output, uint8_t *key)
{
	mbedtls_aes_context aes;

	mbedtls_aes_init(&aes);

	int status = mbedtls_aes_setkey_dec( &aes, key, 128 );
	if ( status )
	{
		log("error: failed to set key with status %d", status);
		goto done;
	}

	status = mbedtls_aes_crypt_ecb( &aes, MBEDTLS_AES_DECRYPT, input, output );
	if ( status )
	{
		log("error: failed to decrypt block with status %d", status);
		goto done;
	}

	status = 0;

done:
	mbedtls_aes_free(&aes);

	return status;
}

int encryptCB(struct cmd_block *cb, uint8_t *ciphertext, uint8_t *key)
{
	struct encrypt_block eb = {0};

	memcpy(eb.data, cb, sizeof(*cb));
	eb.len = sizeof(*cb);
	eb.rand[0] = random();
	eb.rand[1] = random();
	eb.rand[2] = random();

	uint32_t crc = getCRC16((uint8_t*)&eb, sizeof(eb) - 2);
	eb.crc = htons(crc);

	int status = encryptData((byte*)&eb, ciphertext, key);
	if ( status )
	{
		log("error: encryption failed with status %d", status);
		return -1;
	}

	return 0;
}

int decryptCB(struct cmd_block *cb, uint8_t *ciphertext, uint8_t *key)
{
	struct encrypt_block eb = {0};

	int status = decryptData(ciphertext, (byte*)&eb, key);

	if ( status )
	{
		log("error: decryption failed");
		return -1;
	}

	if ( eb.len != sizeof(struct cmd_block) )
	{
		log("error: bad length");
		return -1;
	}

	uint16_t crc = getCRC16(eb.data, 14);

	if ( crc != ntohs(eb.crc) )
	{
		log("error: bad crc");
		return -1;
	}

	memcpy(cb, eb.data, eb.len);

	return 0;
}

void mqttHandleMessage(char* topic, byte* payload, unsigned len)
{
	log("MQTT message received");

	if ( len != 16 )
	{
		log("error: bad message size: %u", len);
		return;
	}

	struct cmd_block cb = {0};
	int error = decryptCB(&cb, payload, config.key);
	if ( error )
	{
		log("error: failed to decrypt cmd with error %d", error);
		return;
	}

	if ( gate_token != ntohs(cb.token) )
	{
		log("error: bad token 0x%X", gate_token);
		return;
	}

	if ( cb.open_req )
		gate_cmd = GATE_CMD_OPEN;
	if ( cb.close_req )
		gate_cmd = GATE_CMD_CLOSE;

	/* clear the token so a new one will be generated */
	if ( gate_cmd != 0 )
		gate_token = 0;
}

boolean mqttConnect(char *client_id)
{
	static uint32_t last_attempt = 0;

	if ( millis() - last_attempt < 5000 )
		return false;
	last_attempt = millis();

	boolean connected = false;

	/* work around an issue where pubsubclient fails to reconnect */
	eth_client.stop();

	log("Creating TCP connection to MQTT broker");
	int eth_connected = eth_client.connect("test.mosquitto.org", 1883);
	if ( ! eth_connected )
		return false;

	log("MQTT connecting with client id %s", client_id);
	if ( mqtt_client.connect(client_id) )
	{
		const char *topic = "house/gate/cmd";
		connected = mqtt_client.subscribe(topic);
		if ( connected == false )
			log("error: subscribe failed");
		else
			log("MQTT subscribed");
	}
	else
	{
		log("MQTT connect failed, state: %d", mqtt_client.state());
	}

	return connected;
}

/* Returns true if connected to MQTT broker */
boolean manageMQTT()
{
	static int inited = 0;
	static char client_id[30];

	if ( ! inited )
	{
		inited = 1;
		log("MQTT initialising");
		mqtt_client.setCallback(mqttHandleMessage);
		mqtt_client.setKeepAlive(60);
		sprintf(client_id, "GateMeister-controller-%lX", random());
	}

	if ( ! mqtt_client.connected() )
	{
		return mqttConnect(client_id);
	}

	int publish_requested = 0;

	if ( gate_token == 0 )
	{
		publish_requested = 1;

		gate_token = micros() * random();

		log("Generating new security token 0x%X", gate_token);
	}

	static int last_gate_state = -1;
	if ( gate_state != last_gate_state )
	{
		last_gate_state = gate_state;
		publish_requested = 1;

		const char *s = "open";
		if ( gate_state == GATE_STATE_CLOSED )
			s = "closed";

		log("Gate state: %s", s);
	}

	if ( publish_requested )
	{
		publish_requested = 0;

		const char *topic = "house/gate/state";

		struct cmd_block cb = {0};
		cb.state_open = gate_state;
		cb.token = htons(gate_token);

		byte buf[16];

		int status = encryptCB( &cb, buf, config.key );

		if ( status )
		{
			log("Failed to encrypt message");
			return true;
		}

		boolean success = mqtt_client.publish(topic, buf, 16, true);
		if ( success )
			log("Published controller state message");
		else
			log("Publishing controller state failed");
	}

	mqtt_client.loop();

	return true;
}

int manageUserButton()
{
	static int btn_user_pressed = 0;
	static uint32_t btn_user_press_tm = 0;

	int pressed = ! digitalRead(BTN_USER);

	if ( btn_user_press_tm == 0 )
	{
		btn_user_press_tm = millis();
		btn_user_pressed = pressed;
	}
	if ( millis() - btn_user_press_tm > 100 )
	{
		if ( pressed != btn_user_pressed )
		{
			btn_user_press_tm = millis() | 1;
			btn_user_pressed = pressed;
			if ( pressed )
				return EVENT_USER_BTN_PRESSED;
			return EVENT_USER_BTN_RELEASED;
		}
	}

	return EVENT_USER_BTN_NONE;
}

void pulseRelay(unsigned relay)
{
	digitalWrite(relay, HIGH);
	delay(50);
	digitalWrite(relay, LOW);
}

void manageHealthLED(boolean gate_open)
{
	static uint32_t timer;
	static uint32_t state;

	switch(state)
	{
		case 0:
			/* green led if gate open, otherwise orange led */
			if ( gate_open )
			{
				digitalWrite(LEDG, HIGH);
			}
			else
			{
				digitalWrite(LEDR, HIGH);
				digitalWrite(LEDG, HIGH);
			}

			if ( millis() - timer > 500 )
			{
				state = 1;
				timer = millis();
			}
			break;
		case 1:
			digitalWrite(LEDR, LOW);
			digitalWrite(LEDG, LOW);
			if ( millis() - timer > 500 )
			{
				state = 0;
				timer = millis();
			}
			break;
	}
}

char *getMAC()
{
	static byte addr[6];
	Ethernet.MACAddress(addr);

	static char addr_str[18];
	for(int i=0; i<6; i++)
	{
		sprintf(addr_str + i * 3, "%02X:", addr[i]);
	}
	addr_str[17] = 0;
	return addr_str;
}

char *getIP()
{
	auto addr = Ethernet.localIP();

	static char addr_str[16];
	for(int i=0; i<4; i++)
	{
		sprintf(addr_str + i * 4, "%03d.", addr[i]);
	}
	addr_str[15] = 0;
	return addr_str;

}

int manageEthernet()
{
	static int connected = 0;

	if ( connected )
	{
		if ( Ethernet.linkStatus() == LinkOFF )
		{
			log("Linkdown detected");
			connected = 0;
		}
		else
		{
			Ethernet.maintain();
		}
	}

	if ( ! connected )
	{
		log("Trying to send DHCP request");
		int status = Ethernet.begin(NULL, 8000, 4000);
		if ( status == 1 )
		{
			randomSeed( micros() );
			log("Ethernet connected with ip %s MAC %s", getIP(), getMAC());
			connected = 1;
		}
		else
		{
			log("Failed to configure Ethernet using DHCP");
		}
	}

	return connected;
}

/*
 * Prepare a modbus exception response
 *
 * Returns the response packet size in bytes
 */
int handleModbusException(uint8_t* buf, modbus_exception_t exception)
{
		buf[1] |= 0x80;
		buf[2] = exception;
		return 3;
}

/*
 * Prepare a modbus response packet in buf for modbus function 3
 * Ignores the first two bytes in the buffer and doesn't handle the CRC
 *
 * Returns the response packet size in bytes
 */
unsigned handleModbusFunc3(uint8_t* buf, uint16_t offset)
{
	unsigned count = buf[4] << 8 | buf[5];

	/* check parameters */
	bool bad_addr  = offset >= MODBUS_NUM_REGS;

	bool bad_count = count < 1 || count > MODBUS_NUM_REGS;

	unsigned last_reg = offset + count - 1;

	if( bad_addr || bad_count || last_reg >= MODBUS_NUM_REGS )
		return handleModbusException(buf, MODBUS_EXCEPTION_BAD_ADDR);

	/* build response */
	buf[2] = count * 2;

	unsigned num_bytes = 3;

	for(unsigned i=0; i<count; i++)
	{
		uint16_t val = 0;

		switch(offset)
		{
			case MODBUS_REG_PROJECT_ID:
				val = 0x7733;
				break;
			case MODBUS_REG_KEY0:
			case MODBUS_REG_KEY1:
			case MODBUS_REG_KEY2:
			case MODBUS_REG_KEY3:
			case MODBUS_REG_KEY4:
			case MODBUS_REG_KEY5:
			case MODBUS_REG_KEY6:
			case MODBUS_REG_KEY7:
			{
				unsigned index = offset - MODBUS_REG_KEY0;
				auto reg = (uint16_t*) (config.key + index * 2);
				val = *reg;
				break;
			}
		}

		buf[num_bytes++] = highByte(val);
		buf[num_bytes++] = lowByte(val);

		offset++;
	}

	return num_bytes;
}

/*
 * Handle any incoming modbus requests
 */
void manageModbusTCP()
{
	static int initialised = 0;

	if ( ! initialised )
	{
		log("Modbus TCP initialised");
		initialised = 1;
		server = EthernetServer(502);
		server.begin();
	}

	EthernetClient client = server.available();

	if( client )
	{
		log("Modbus client connected");
		const unsigned PKT_BUF_SIZE = 6 + 1 + 253;  //header + addr + modbus-pkt

		uint8_t buf[PKT_BUF_SIZE];

		unsigned num_bytes = 0;

		/* wait for data */
		uint32_t tm = millis();
		while( millis() - tm < 200 )
		{
			if( client.available() )
				break;
			delay(1);
		}

		/* read data */
		while( client.available() )
		{
			buf[num_bytes++] = client.read();

			if( num_bytes >= PKT_BUF_SIZE - 2 )
				break;

			if( ! client.available() )
				delay(5);
		}

		if( num_bytes > 6 )
		{
			/* skip MBAP header */
			num_bytes -= 6;

			uint8_t* pkt = buf+6;

			if( num_bytes >= 6 ) //min packet size
			{
				/* parse packet data */
				uint8_t  func     = pkt[1];
				uint16_t offset   = pkt[2] << 8 | pkt[3];

				/* generate response in pkt */
				if ( func == 3 && num_bytes == 6 )
					num_bytes = handleModbusFunc3(pkt, offset);
				else
					num_bytes = handleModbusException(pkt, MODBUS_EXCEPTION_BAD_FUNC);

				if( num_bytes > 0 )
				{
					/* update length field in MBAP header */
					uint16_t len = num_bytes;
					buf[4] = highByte(len);
					buf[5] = lowByte(len);

					client.write(buf, num_bytes+6);
				}
			}
		}
	}
}

void manageGate()
{
	if ( gate_cmd == GATE_CMD_OPEN )
	{
		log("Opening Gate");
		pulseRelay(RELAY1);
	}

	if ( gate_cmd == GATE_CMD_CLOSE )
	{
		log("Closing Gate");
		pulseRelay(RELAY2);
	}

	gate_cmd = GATE_CMD_NONE;
}

int loadConfig(struct config *c)
{
	int status = eeprom_read((byte*)c, sizeof(*c));

	if ( status )
	{
		log("loadConfig() read failed with status %d", status);
	}
	else
	{
		uint16_t crc = getCRC16((uint8_t*)c, sizeof(*c) - 2);
		if ( crc != c->crc )
		{
			log("loadConfig() bad crc");
			status = -1;
		}
	}

	log("%s completed with status %d", __func__, status);

	return status;
}

int saveConfig(struct config *c)
{
	auto b = (byte*) c;

	c->crc = getCRC16(b, sizeof(*c) - 2);
	return eeprom_write(b, sizeof(*c));
}

int setDefConfig(struct config *c)
{
	c->cfg_ver = 1;
	log("%s saving default config", __func__);

	return saveConfig(c);
}

void generateKey(struct config *c)
{
	int key_bytes = 0;

	log("Click User button until key is generated");

	while( key_bytes < 16 )
	{
		if ( manageUserButton() == EVENT_USER_BTN_PRESSED )
		{
			c->key[key_bytes] = micros();
			log("key[%d] = 0x%X", key_bytes, c->key[key_bytes]);
			key_bytes++;
		}
	}
}

void setup()
{
	pinMode(A7,     INPUT);
	pinMode(RELAY1, OUTPUT);
	pinMode(RELAY2, OUTPUT);
	pinMode(RELAY3, OUTPUT);
	pinMode(RELAY4,   OUTPUT);
	pinMode(BTN_USER, INPUT);
	pinMode(LEDR,        OUTPUT);
	pinMode(LED_D0,      OUTPUT);
	pinMode(LED_D1,      OUTPUT);
	pinMode(LED_D2,      OUTPUT);
	pinMode(LED_D3,      OUTPUT);
	pinMode(LED_BUILTIN, OUTPUT);

	digitalWrite(LED_D0, LOW);
	digitalWrite(LED_D1, LOW);
	digitalWrite(LED_D2, LOW);
	digitalWrite(LED_D3, LOW);

	Serial.begin(9600);

	while (!Serial) {}
	for(int i=0; i<30; i++)
		Serial.println(" ");
	log("Serial port ready");

	digitalWrite(LED_D0, HIGH); /* enable led when setup started */

	log("Initialising EEPROM");
	int status = eeprom_init();
	if ( status )
		log("eeprom_init() failed with status %d", status);

	log("Loading config");
	status = loadConfig(&config);
	if ( status )
	{
		generateKey(&config);

		setDefConfig(&config);
	}

	status = eeprom_deinit();
	if ( status )
		log("eeprom_deinit() failed with status %d", status);

	digitalWrite(LED_D1, HIGH); /* enable led when encryption key setup */

	char key[32];
	for(int i=0; i<16; i++)
		sprintf(key + i * 2, "%02X", config.key[i]);
	log("key: 0x%s", key);

	log("Setup done");
}

void loop()
{
	gate_state = (enum gateState) digitalRead(A7);

	int btn_event = manageUserButton();
	if ( btn_event == EVENT_USER_BTN_PRESSED )
	{
		log("Button pressed");
		pulseRelay(RELAY1);
	}
	if ( btn_event == EVENT_USER_BTN_RELEASED )
	{
		log("Button released");
		pulseRelay(RELAY2);
	}

	manageHealthLED(gate_state);

	int connected = manageEthernet();

	digitalWrite(LED_D2, connected); /* enable led when Ethernet connected */

	if ( connected )
	{
		manageModbusTCP();

		connected = manageMQTT();
	}

	digitalWrite(LED_D3, connected); /* enable led when MQTT connected */

	manageGate();
}

